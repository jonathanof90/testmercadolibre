import React from 'react';

const Breadcrumb = ({ bcLinks }) => {
  return (
    <section className="breadcrumb col-12 col-sm-12">
      <ul>
        {bcLinks.map(link => (
          <li key={link}>{link}</li>
        ))}
      </ul>
    </section>
  );
};

export default Breadcrumb;
