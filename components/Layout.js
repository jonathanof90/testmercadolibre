import React, { Fragment } from 'react';
import SearchBar from './SearchBar';

import 'bootstrap-scss/bootstrap.scss';
import '../static/scss/components/main.scss';

const Layout = ({ children }) => (
  <Fragment>
    <SearchBar />
    <main className="container-fluid mainContainer">
      <div className="row justify-content-center">
        <div className="col-12 col-sm-10">{children}</div>
      </div>
    </main>
  </Fragment>
);

export default Layout;
