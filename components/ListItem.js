import React from 'react';
import Link from 'next/link';

const ListItem = ({ price, picture, shipping, title, id }) => {
  return (
    <Link href={`/items/${id}`} as={`/items/${id}`}>
      <div className="col-12 listItem">
        <img alt="item" className="itemImage" src={picture} />
        <div className="itemInfo">
          <h2 className="itemPrice">
            {price}
            {shipping ?
              <img
                className="shipping"
                src="/static/img/ic_shipping.png"
                srcSet="/static/img/ic_shipping@2x.png 2x"
                alt="Free Shipping"
              />
             : ''}
          </h2>
          <h3 className="itemTitle">{title}</h3>
        </div>
      </div>
    </Link>
  );
};
export default ListItem;
