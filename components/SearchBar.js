import React, { Component } from 'react';
import Router from 'next/router';

class SearchBar extends Component {

  state = {
    value: ''
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  doSearch = (event) => {
    const { value } = this.state;
    event.preventDefault();
    return Router.push(`/items?search=${value}`);
  }

  render() {
    const { value } = this.state;
    return (
      <header className="searchBar container-fluid">
        <div className="row justify-content-center">
          <div className="col-12 col-sm-10 inputContainer">
            <a href="/" className="mainLogo">
              <img
                src="/static/img/Logo_ML.png"
                srcSet="/static/img/Logo_ML@2x.png 2x"
                alt="Mercado Libre"
              />
            </a>
            <form className="input-group" onSubmit={this.doSearch}>
              <input
                type="search"
                className="form-control"
                placeholder="Nunca dejes de buscar"
                value={value}
                onChange={this.handleChange}
              />
              <div className="input-group-append">
                <button className="btn btn-secondary searchButton" type="button">
                  <img
                    src="/static/img/ic_Search.png"
                    srcSet="/static/img/ic_Search@2x.png 2x"
                    alt="search"
                  />
                </button>
              </div>
            </form>
          </div>
        </div>
      </header>
    );
  }
}
export default SearchBar;
