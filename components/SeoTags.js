import React from 'react';
import NextSeo from 'next-seo';

const SeoTags = ({ title, imgUrl }) => (
  <NextSeo
    config={{
      title: title,
      description: 'Front end test form MercadoLibre',
      canonical: 'https://www.canonical.ie/',
      openGraph: {
        url: 'https://www.url.ie/a',
        title: 'Open Graph Title',
        description: 'Open Graph Description',
        images: [
          {
            url: `${imgUrl}`,
            width: 800,
            height: 600,
            alt: `${title} alt img`
          },
          {
            url: `${imgUrl}`,
            width: 900,
            height: 800,
            alt: `${title} second img`
          },
          { url: `${imgUrl}` },
          { url: `${imgUrl}` }
        ],
        site_name: 'FrontEnd Test Mercadolibre'
      },
      twitter: {
        handle: '@handle',
        site: '@site',
        cardType: 'summary_large_image'
      }
    }}
  />
);
export default SeoTags;
