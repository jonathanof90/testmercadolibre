import React from 'react';

const SingleItem = ({ description, price, sold_quantity, condition, title, picture }) => (
  <div className="col-12 singleItem">
    <div className="row">
      <div className="col-12 col-sm-8">
        <img alt="item" className="img-fluid" src={picture} />
        <div className="itemDescription d-none d-sm-block">
          <h3 className="descTitle">Descripcion del producto</h3>
          <p>{description}</p>
        </div>
      </div>
      <div className="col-12 col-sm-4 itemInfo">
        <h4 className="soldCount">
          {condition === 'used' ? 'Usado' : 'Nuevo'} - {sold_quantity} vendidos
        </h4>
        <h3 className="itemTitle">{title}</h3>
        <h2 className="itemPrice">{price}
          <img
            className="shipping"
            src="/static/img/ic_shipping.png"
            srcSet="/static/img/ic_shipping@2x.png 2x"
            alt="Free Shipping"
          />
        </h2>
        <button type="button" className="btn btn-primary">Comprar</button>
      </div>
    </div>
    <div className="row">
      <div className="col-12 d-block d-sm-none itemDescription">
        <h3 className="descTitle">Descripcion del producto</h3>
        <p>{description}</p>
      </div>
    </div>
  </div>
);

export default SingleItem;
