import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <link
            rel="shortcut icon"
            href="https://http2.mlstatic.com/ui/navigation/4.4.2/mercadolibre/favicon.ico"
          />
          <link
            rel="apple-touch-icon"
            href="https://http2.mlstatic.com/ui/navigation/4.4.2/mercadolibre/60x60-precomposed.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="https://http2.mlstatic.com/ui/navigation/4.4.2/mercadolibre/76x76-precomposed.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="https://http2.mlstatic.com/ui/navigation/4.4.2/mercadolibre/120x120-precomposed.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="https://http2.mlstatic.com/ui/navigation/4.4.2/mercadolibre/152x152-precomposed.png"
          />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="HandheldFriendly" content="True" />
          <meta httpEquiv="cleartype" content="on" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
