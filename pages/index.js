import React, { Fragment } from 'react';
import Head from 'next/head';

const Index = () => (
  <Fragment>
    <Head>
      <title>Frontend Test | Mercadolibre</title>
      <meta name="description" content="Test para Mercadolibre por Jonathan Fernandez" />
    </Head>
  </Fragment>
);
export default Index;
