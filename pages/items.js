import React, { Component , Fragment} from 'react';
import axios from 'axios';
import ListItem from '../components/ListItem';
import SingleItem from '../components/SingleItem';
import Breadcrumb from '../components/Breadcrumb';
import SeoTags from '../components/SeoTags';

class Items extends Component {
  static async getInitialProps(req) {
    if (req.query.search) {
      const searchData = await axios.get(`http://localhost:3000/api/items?q=${req.query.search}`);
      const innerData = await searchData.data;
      const search = req.query.search;
      return { ...innerData, search};
    }
    if (req.query.id) {
      const itemData = await axios.get(`http://localhost:3000/api/items/${req.query.id}`);
      const innerData = itemData.data;
      return { ...innerData };
    }
    return {};
  }
  renderItems = () => {
    if (this.props.items) {
      const {items, category, search} = this.props;
      return (
        <Fragment>
          <section className="row">
            <Breadcrumb bcLinks={category}/>
            <SeoTags title={`${search} - ${category[0]}`} imgUrl={items[0].picture} />
          </section>
          <section className="itemContainer row">
            {items.map(i => <ListItem
                  id={i.id}
                  key={i.id}
                  price={i.price.ammount.toLocaleString('es-ar', {
                      style: 'currency',
                      currency: 'ARS',
                      minimumFractionDigits: 0,
                      maximumFractionDigits: 0,
                  })}
                  title={i.title}
                  picture={i.picture}
                  shipping={i.free_shipping}
                />
            )}
          </section>
        </Fragment>
      );
    }
    if (this.props.item) {
      const {item, category} = this.props;
      return (
        <Fragment>
          <SeoTags title={`${item.title} - ${category[0]}`} imgUrl={item.picture} />
          <section className="row">
            <Breadcrumb bcLinks={category}/>
          </section>
          <section className="itemContainer row">
            <SingleItem 
              description={item.description}
              price={item.price.ammount.toLocaleString('es-ar', {
                  style: 'currency',
                  currency: 'ARS',
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0,
              })}
              sold_quantity={item.sold_quantity}
              condition={item.condition}
              title={item.title}
              picture={item.picture}
            />
          </section>
      </Fragment>
      )
    };
    return (
      <Fragment>
        <SeoTags title="Frontend Test | Mercadolibre" imgUrl="" />
      </Fragment>
    );
  }
  render() {
    return this.renderItems();
  }
}

export default Items;
