const express = require('express');
const next = require('next');
const axios = require('axios');
const robots = require('express-robots-txt');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const apiUrl = 'https://api.mercadolibre.com/';
const signature = {
  author: {
    name: 'Jonathan',
    lastname: 'Fernandez'
  }
};

const getCategory = async id => {
  const { data } = await axios.get(`${apiUrl}categories/${id}`);
  const categories = await data.path_from_root.map(cat => cat.name);
  return categories;
};

const getItemList = async term => {
  const { data } = await axios
    .get(`${apiUrl}sites/MLA/search?q=${term}&limit=4`)
    .catch(error => console.log(error));
  let listCat;
  if (data.filters.length > 0) {
    listCat = data.filters
      .find(f => f.id === 'category')
      .values[0].path_from_root.map(cat => cat.name);
  } else {
    listCat = data.available_filters
      .find(f => f.id === 'category')
      .values.sort((a, b) => (a.results > b.results ? -1 : 1))
      .slice(0, 4)
      .map(cat => cat.name);
  }
  const items = data.results.map(item => {
    return {
      id: item.id,
      title: item.title,
      price: { currency: item.currency_id, ammount: item.price, decimals: 2 },
      picture: item.thumbnail,
      condition: item.condition,
      free_shipping: item.shipping.free_shipping
    };
  });
  return { category: listCat, items };
};

const getSingleItem = async id => {
  const [item, description] = await Promise.all([
    axios.get(`${apiUrl}items/${id}`),
    axios.get(`${apiUrl}items/${id}/description`)
  ])
  const category = await getCategory(item.data.category_id);
  const formattedItem = {
    id: item.data.id,
    title: item.data.title,
    price: {
      currency: item.data.currency_id,
      ammount: item.data.price,
      // No se de donde sacar decimals, asi que lo hardcodeo.
      decimals: 2
    },
    picture: item.data.thumbnail,
    condition: item.data.condition,
    free_shipping: item.data.shipping.free_shipping,
    sold_quantity: item.data.sold_quantity,
    description: description.data.plain_text
  };
  return { category, formattedItem };
};

app.prepare().then(() => {
  const server = express();
  server.use(robots({ UserAgent: '*', Disallow: '/_next/' }));
  // Server routes
  server.get('/items', (req, res) => {
    if (req.query.search) {
      return app.render(req, res, '/items', { search: req.query.search });
    }
    return app.render(req, res, '/items');
  });

  server.get('/items/:id', (req, res) => {
    return app.render(req, res, '/items', { id: req.params.id });
  });

  // Api routes
  server.get('/api/items', async (req, res) => {
    if (req.query.q) {
      const list = await getItemList(req.query.q);
      return res.send({ ...signature, ...list });
    }
    return {};
  });

  server.get('/api/items/:id', async (req, res) => {
    if (req.params.id) {
      const item = await getSingleItem(req.params.id);
      return res.send({
        ...signature,
        category: item.category,
        item: item.formattedItem
      });
    }
    return {};
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });
  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
